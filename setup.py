#!/usr/bin/env python

import sys, os, getopt
from distutils.core import setup, Extension

version_string = "1.2.8"

if sys.version_info[:2] < (2,2):
	print "Sorry, rdiff-backup requires version 2.2 or later of python"
	sys.exit(1)

# Defaults
lflags_arg = []
libname = ['rsync']
incdir_list = libdir_list = None
extra_options = {}
long_description = """rdiff-backup backs up one directory to another, possibly over a network. The
target directory ends up a copy of the source directory, but extra reverse
diffs are stored in a special subdirectory of that target directory, so you
can still recover files lost some time ago. The idea is to combine the best
features of a mirror and an incremental backup. rdiff-backup also preserves
subdirectories, hard links, dev files, permissions, uid/gid ownership,
modification times, extended attributes, acls, and resource forks.

Also, rdiff-backup can operate in a bandwidth efficient manner over a
pipe, like rsync. Thus you can use rdiff-backup and ssh to securely
back a hard drive up to a remote location, and only the differences
will be transmitted. Finally, rdiff-backup is easy to use and settings
have sensible defaults.
"""

if os.name == 'posix' or os.name == 'nt':
	LIBRSYNC_DIR = os.environ.get('LIBRSYNC_DIR', '')
	LFLAGS = os.environ.get('LFLAGS', [])
	LIBS = os.environ.get('LIBS', [])

	# Handle --librsync-dir=[PATH] and --lflags=[FLAGS]
	args = sys.argv[:]
	for arg in args:
		if arg.startswith('--librsync-dir='):
			LIBRSYNC_DIR = arg.split('=')[1]
			sys.argv.remove(arg)
		elif arg.startswith('--lflags='):
			LFLAGS = arg.split('=')[1].split()
			sys.argv.remove(arg)
		elif arg.startswith('--libs='):
			LIBS = arg.split('=')[1].split()
			sys.argv.remove(arg)

		if LFLAGS or LIBS:
			lflags_arg = LFLAGS + LIBS

		if LIBRSYNC_DIR:
			incdir_list = [os.path.join(LIBRSYNC_DIR, 'include')]
			libdir_list = [os.path.join(LIBRSYNC_DIR, 'lib')]
		if '-lrsync' in LIBS:
			libname = []

if os.name == 'nt':
	try:
		import py2exe
	except ImportError:
		pass
	else:
		extra_options = {
			'console': ['rdiff-backup'],
		}
		if '--single-file' in sys.argv[1:]:
			sys.argv.remove('--single-file')
			extra_options.update({
				'options': {'py2exe': {'bundle_files': 1}},
				'zipfile': None
			})

setup(name="rdiff-backup",
	  version=version_string,
	  description="Local/remote mirroring+incremental backup",
	  long_description=long_description,
	  author="Ben Escoto",
	  author_email="rdiff-backup@emerose.org",
	  license="GNU General Public License v2 (GPLv2)",
	  url="http://rdiff-backup.nongnu.org/",
	  packages = ['rdiff_backup'],
	  ext_modules = [Extension("rdiff_backup.C", ["cmodule.c"]),
					 Extension("rdiff_backup._librsync",
							   ["_librsyncmodule.c"],
							   include_dirs=incdir_list,
							   library_dirs=libdir_list,
							   libraries=libname,
							   extra_link_args=lflags_arg)],
	  scripts = ['rdiff-backup', 'rdiff-backup-statistics'],
	  data_files = [('share/doc/rdiff-backup-%s' % (version_string,),
					 ['CHANGELOG', 'COPYING', 'README', 'FAQ.html'])],
					**extra_options)

